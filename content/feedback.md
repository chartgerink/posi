---
title: Feedback
subtitle: Please share your thoughts
date: "2020-11-26"
publishdate: "2020-11-20"
comments: false
---

Please submit a [GitLab issue](https://gitlab.com/crossref/posi/-/issues) to make any comments, share ideas, or to ask questions about "The Principles of Open Scholarly Infrastructure" or about this website. Thanks.

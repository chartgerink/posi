---
title: Posse
subtitle: Who has committed to the POSI principles?
date: "2022-03-09"
publishdate: "2021-03-09"
comments: false
---


These organizations or initiatives (listed alphabetically) have formally adopted the POSI principles by publishing an initial self-audit, and committed to routinely demonstrating evidence of following POSI in practice. 


- Crossref: [POSI fan tutte](https://www.crossref.org/blog/posi-fan-tutte/) (2022-March-08) and [original](https://www.crossref.org/blog/crossrefs-board-votes-to-adopt-the-principles-of-open-scholarly-infrastructure) (2020-December-02)
- [DataCite](https://doi.org/10.5438/vy7h-g464) (original posted 2021-August-30)
- [Dryad](https://blog.datadryad.org/2020/12/08/dryads-commitment-to-the-principles-of-open-scholarly-infrastructure/) (original posted 2020-December-08)
- [Europe PMC](http://blog.europepmc.org/2022/02/EPMC-adopts-POSI.html) (original posted 2022-February-21)
- [JOSS](https://blog.joss.theoj.org/2021/02/JOSS-POSI) (original posted 2021-February-14)
- [OA Switchboard](https://www.oaswitchboard.org/blog7oct2021) (original posted 2021-October-07)
- [OpenAIRE](https://www.openaire.eu/principles-open-scholarly-infrastructure-openaire-assessment) (original posted 2022-March-29)
- [OpenCitations](https://opencitations.wordpress.com/2021/08/09/opencitations-compliance-with-the-principles-of-open-scholarly-infrastructure/) (original posted 2021-August-09)
- [OurResearch](https://blog.ourresearch.org/posi/) (original posted 2021-June-10)
- [ROR](https://ror.org/blog/2020-12-16-aligning-ror-with-posi/) (original posted 2020-December-16)
- [Sciety](https://blog.sciety.org/open-scholarly-infrastructure/) (original posted 2021-November-22)

> If you are considering adopting POSI and would like to chat about it, please reach out to any colleagues at any of the organizations above.

---

The following key documents (listed chronologically) discuss or draw on the POSI principles:

| Year & month	| Title	| Authors |
|--------------------------	|---	|---|
| 2015-February	| (Originally-proposed) [Principles for Open Scholarly Infrastructures](http://dx.doi.org/10.6084/m9.figshare.1314859)	| Neylon, Bilder, Lin |
| 2015-August	| [What exactly is infrastructure? Seeing the leopard's spots](http://dx.doi.org/10.6084/m9.figshare.1520432) 	| Neylon, Bilder, Lin |
| 2016-January	| [Where are the pipes? Building Foundational Infrastructures for Future Services](http://cameronneylon.net/blog/where-are-the-pipes-building-foundational-infrastructures-for-future-services/) 	| Neylon, Bilder, Lin |
| 2016-July	| [Squaring Circles: The economics and governance of scholarly infrastructures](http://cameronneylon.net/blog/squaring-circles-the-economics-and-governance-of-scholarly-infrastructures/)	| Neylon |
| 2018-April	| [Supporting Research Communications: A Guide](https://doi.org/10.5281/zenodo.3524663)	| Chodacki et al |
| 2019-January	| [Good Practice Principles for Scholarly Communication Services](https://sparcopen.org/our-work/good-practice-principles-for-scholarly-communication-services/) 	| COAR-SPARC |
| 2019-April	| [EXAMPLARITY CRITERIA for funding from the National Open Science Fund through platforms, infrastructures and editorial content](https://www.ouvrirlascience.fr/examplarity-criteria-for-funding-from-the-national-open-science-fund/) 	| French National Open Science Fund |
| 2020-October	| [Living Our Values and Principles: Exploring Assessment Strategies for the Scholarly Communication Field](https://educopia.org/living-our-values-and-principles/) 	| Skinner and Wipperman, Educopia Institute |
| 2021-October	| [Now is the time to work together toward open infrastructures for scholarly metadata](https://blogs.lse.ac.uk/impactofsocialsciences/2021/10/27/now-is-the-time-to-work-together-toward-open-infrastructures-for-scholarly-metadata/) 	| Hendricks et al |
| 2021-November	| [Beyond open: Key criteria to assess open infrastructure](https://investinopen.org/blog/criteria-to-assess-openinfra/) 	| Invest In Open |
| 2022-February	| [Assessing data infrastructure: the Principles of Open Scholarly Infrastructure](https://blog.ldodds.com/2022/02/23/assessing-data-infrastructure-the-principles-of-open-scholarly-infrastructure/) 	| Dodds |
 

---
If you spot something missing from this page please [open a ticket](https://gitlab.com/crossref/posi/-/issues) to add to the list.
